package com.nrs.ws.soap;

import com.nrs.entities.ExclusionSpecialistRequests;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.sql.Date;

/**
 * Created by pioner on 25.04.17.
 */
@Service
public class ExclusionSpecialistService
{
    private static final String TYPE_REQUEST = "exclusion";

    static final Logger exclusionServiceLogger = LogManager.getLogger(ExclusionSpecialistService.class);

    public static final int ERROR_CODE_SPECIALIST_NOT_FOUND = 2006;

    /**
     *
     * @param session
     * @param idNostroyRequest
     * @param idSpecialist
     * @param sroNumber
     * @param applicationSroDate
     * @return
     * @throws Exception
     */
    public int exclusionSpecialist(Session session, int idNostroyRequest, int idSpecialist,String sroNumber, Date applicationSroDate) throws Exception
    {
        int idRequest;
        try
        {
            session.beginTransaction();

            ExclusionSpecialistRequests nsRequest = new ExclusionSpecialistRequests();

            Calendar calendar = Calendar.getInstance();
            nsRequest.setCreated(new Timestamp(calendar.getTime().getTime()));
            nsRequest.setUpdated(new Timestamp(calendar.getTime().getTime()));

            NostroyRequests nostroyRequest = (NostroyRequests) session.get(NostroyRequests.class, idNostroyRequest);
            Specialists specialist = (Specialists) session.get(Specialists.class, idSpecialist);
            nsRequest.setNostroyRequest(nostroyRequest);
            nsRequest.setType(TYPE_REQUEST);
            nsRequest.setSroNumber(sroNumber);
            nsRequest.setApplicationSroDate(applicationSroDate);
            nsRequest.setSpecialist(specialist);
            session.save(nsRequest);
            session.getTransaction().commit();
            idRequest = nsRequest.getId();

            return idRequest;
        } catch(Exception e){
            throw new Exception(e.getMessage());
        } catch (Throwable  t) {
            throw new Exception("Internal error. "+t.getMessage());
        }
    }

    /**
     *
     * @param session
     * @param user
     * @param nostroyRequestId
     * @param specialistId
     * @throws Exception
     */
    public void checkRequestByUser(Session session, Users user, int nostroyRequestId, int specialistId) throws Exception
    {
        try {
            NostroyRequests ns = (NostroyRequests) session.get(NostroyRequests.class, nostroyRequestId);

            if (ns == null) {
                throw new Exception("ERROR:" + InclusionSpecialistService.ERROR_CODE_REQUEST_NOT_FOUND + ";" + "Request not found");
            }

            Specialists sp = (Specialists)session.get(Specialists.class, specialistId);

            if (sp == null) {
                throw new Exception("ERROR:" + ERROR_CODE_SPECIALIST_NOT_FOUND + ";" + "Specialist not found");
            }

            if (ns.getStatus().getId() != InclusionSpecialistService.DRAFT_STATUS) {
                throw new Exception("ERROR:" + InclusionSpecialistService.ERROR_CODE_REQUEST_STATUS + ";" + "Status request failed");
            }
            if (ns.getUser().getId() != user.getId()) {
                throw new Exception("ERROR:" + InclusionSpecialistService.ERROR_CODE_REQUEST_OWNER + ";" + "Owner request failed");
            }
        } catch(Exception e){
            throw new Exception(e.getMessage());
        } catch (Throwable  t) {
            throw new Exception("Internal error. "+t.getMessage());
        }
    }
}
