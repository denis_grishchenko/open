package com.nrs.ws.soap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.ws.soap.server.endpoint.interceptor.SoapEnvelopeLoggingInterceptor;

/**
 * Created by pioner on 02.06.17.
 */
public class SysErrLoggingInterceptor extends SoapEnvelopeLoggingInterceptor {
    private static final Logger messageServiceLogger = LogManager.getLogger(SysErrLoggingInterceptor.class);

    @Override
    protected void logMessage(String message) {
        messageServiceLogger.debug(message);
    }
}
