package com.nrs.ws.soap;


import com.nrs.entities.*;
import com.nrs.ws.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.core.context.SecurityContextHolder;
import java.security.Principal;
import java.sql.Date;
import java.util.List;
import com.nrs.ws.exceptions.RequestWebserviceException;

@Service
public class StubNoprizService implements NoprizService
{
    private static final Logger stubServiceLogger = LogManager.getLogger(StubNoprizService.class);
    private static final int ERROR_CODE_AUTH_USER_EMPTY = 2001;
    private static final int ERROR_CODE_AUTH_USER_NOT_FOUND = 2002;

    public static final String ACTION_CHANGE = "change";
    public static final String ACTION_EXCLUSION = "exclusion";

    private ExclusionSpecialistService exlusionService;


    @Autowired
    private SessionFactory sessionFactory;

    protected Session getSession()
    {
        try {
            return sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            return sessionFactory.openSession();
        }
    }
    @Autowired
    public StubNoprizService(ExclusionSpecialistService exlusionService) {
        this.exlusionService = exlusionService;
    }


    /**
     *
     * @param idSpecialist
     * @param sroNumber
     * @param applicationSroDate
     * @return
     * @throws RequestWebserviceException
     */
    public  ExclusionSpecialistResponse exclusionSpecialistRequest( int idSpecialist, String sroNumber, Date applicationSroDate) throws RequestWebserviceException
    {
        Session session = getSession();
        try {
            Users user =  getCurrentUser(session);
            int idRequest = 0;
            exlusionService.checkRequestByUser(session, user,  idRequest, idSpecialist);
            int idSpecialistRequest = exlusionService.exclusionSpecialist(session, idRequest, idSpecialist, sroNumber, applicationSroDate);

            ExclusionSpecialistResponse request = new ExclusionSpecialistResponse();

            request.setIdSpecialistRequest(idSpecialistRequest);

            return request;
        } catch (Exception e) {
            String msgError = e.getMessage();
            if(null == msgError) {
                stubServiceLogger.fatal("trace message:" + e.toString());
                msgError = new String("Internal error");
            }
            stubServiceLogger.fatal("fatal error message:" + msgError);
            throw new RequestWebserviceException(msgError);
        }
    }



    /**
     *
     * @param session
     * @return
     * @throws RequestWebserviceException
     */
    private Users getCurrentUser(Session session) throws RequestWebserviceException
    {
    }
}
