package com.nrs.ws.soap;

import com.nrs.ws.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import com.nrs.ws.exceptions.RequestWebserviceException;

import java.sql.Date;

@Endpoint
public class NoprizEndpoint {

    private static final String NAMESPACE_URI = "http://soap.ws.nrs.com";
    private  NoprizService noprizService;

    @Autowired
    public NoprizEndpoint(NoprizService noprizService) {
        this.noprizService = noprizService;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "exclusionSpecialistRequestRequest")
    @ResponsePayload
    public ExclusionSpecialistResponse exclusionSpecialistRequest(@RequestPayload ExclusionSpecialistRequest exclusionSpecialistRequest) throws RequestWebserviceException
    {
        java.util.Date dt = exclusionSpecialistRequest.getApplicationSroDate().toGregorianCalendar().getTime();
        Date applicationSroDate = new java.sql.Date(dt.getTime());

        return noprizService.exclusionSpecialistRequest(
                exclusionSpecialistRequest.getIdSpecialist(), exclusionSpecialistRequest.getSroNumber(),
                applicationSroDate);
    }
}
