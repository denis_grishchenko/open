package com.nrs.ws.soap;

import com.nrs.ws.model.*;
import com.nrs.ws.exceptions.RequestWebserviceException;

import java.sql.Date;

/**
 * Created by pioner on 12.04.17.
 */
public interface NoprizService {

    ExclusionSpecialistResponse exclusionSpecialistRequest(int idSpecialist, String sroNumber, Date applicationSroDate) throws RequestWebserviceException;

}