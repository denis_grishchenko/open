package com.nrs.ws.exceptions;

import org.springframework.ui.context.Theme;
import org.springframework.ws.soap.server.endpoint.annotation.FaultCode;
import org.springframework.ws.soap.server.endpoint.annotation.SoapFault;

@SoapFault(faultCode = FaultCode.SERVER)
public class RequestWebserviceException  extends WSException {

    public RequestWebserviceException(String message) {
        super(message);
    }

    public RequestWebserviceException(String message, Throwable cause) {
        super(message, cause);
    }

}
