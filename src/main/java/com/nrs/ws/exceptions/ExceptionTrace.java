package com.nrs.ws.exceptions;

/**
 * Created by pioner on 11.04.17.
 */
public class ExceptionTrace {
    private String trace;

    public String getTrace() {
        return trace;
    }

    public void setTrace(String trace) {
        this.trace = trace;
    }
}
