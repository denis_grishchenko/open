package com.nrs.ws.exceptions;

/**
 * Created by pioner on 11.04.17.
 */
public class WSException extends Exception {

    private ExceptionTrace exceptionTrace = new ExceptionTrace();

    public WSException(String message) {
        super(message);
    }

    public WSException(String message, Throwable cause) {
        super(message, cause);
    }

    public WSException(Throwable cause) {
        super(cause);
    }

    public ExceptionTrace getExceptionTrace() {
        exceptionTrace.setTrace(getStringFromTrace(getStackTrace()));
        return exceptionTrace;
    }

    private String getStringFromTrace(StackTraceElement[] stackTrace) {
        StringBuilder builder = new StringBuilder();
        builder.append("\n");
        for (StackTraceElement element : stackTrace) {
            builder.append(String.valueOf(element)).append("\n");
        }
        return builder.toString();
    }
}

