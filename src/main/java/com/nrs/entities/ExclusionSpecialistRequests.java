package com.nrs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by pioner on 25.04.17.
 */
@Entity
@Table(name = "exclusion_specialist_requests", schema = "sr")
public class ExclusionSpecialistRequests extends SpecialistRequests {}
